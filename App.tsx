import React from 'react';
import {SafeAreaView, StatusBar, StyleSheet} from 'react-native';
import {store} from './src/redux';
import {Provider} from 'react-redux';
import {AppWrapper} from './src/routes';

const App = () => {
  return (
    <Provider store={store}>
      <SafeAreaView style={styles.container}>
        <AppWrapper />
      </SafeAreaView>
    </Provider>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight,
  },
});
