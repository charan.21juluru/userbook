import {getHeaderInfo} from '../helpers';
import {removeTokens} from './asyncStorage';

import axios from 'react-native-axios';

const handleResponse = (response: any) => {
  if (response.status === 401) {
    removeTokens();
  }
  if (response.data.status !== 'OK') {
    return response.data;
  }

  return response;
};

export const post = async (url: any, body: any) => {
  const header = await getHeaderInfo();
  try {
    const resp = await axios.post(url, body, header);
    return handleResponse(resp);
  } catch (err: any) {
    throw handleResponse(err.response);
  }
};

export const get = async (url: any, params: any = {}) => {
  const header = await getHeaderInfo();
  try {
    const resp = await axios.get(url, {...header, params});
    return handleResponse(resp);
  } catch (err: any) {
    throw handleResponse(err.response);
  }
};
