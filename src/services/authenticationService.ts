import {getAccessToken} from './asyncStorage';
import {env_var} from '.././config';
import {post} from './apiService';

export interface AuthPayload {
  email: string;
  password: string;
}

export const authenticate = async (data: AuthPayload) => {
  const response = await post(`${env_var.BASE_URL}login`, data);
  return {
    status: true,
    data: {
      response,
    },
  };
};

export const isAuthenticated = async (): Promise<boolean> => {
  const token = await getAccessToken();
  return token !== null ? true : false;
};
