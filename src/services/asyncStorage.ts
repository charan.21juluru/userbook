import AsyncStorage from '@react-native-async-storage/async-storage';

export const setTokens = (authRes: any) => {
  AsyncStorage.setItem('token', JSON.stringify(authRes.token));
};

export const removeTokens = () => {
  AsyncStorage.removeItem('token');
};

export const getAccessToken = async () => await AsyncStorage.getItem('token');
export const getUser = () => AsyncStorage.getItem('user');
export const setUser = (user: any) =>
  AsyncStorage.setItem('user', JSON.stringify(user));
