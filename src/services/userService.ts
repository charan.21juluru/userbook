import {get} from './apiService';
import {env_var} from '../config';

export const getUserList = async () => {
  const userList = await get(`${env_var.BASE_URL}users?delay=20`);
  return userList;
};

export const getUserDetails = async (user_id: number) => {
  const user = await get(`${env_var.BASE_URL}users/${user_id}`);
  return user;
};
