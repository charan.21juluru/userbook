import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {authenticate} from '../../services/authenticationService';
import {setTokens} from '../../services/asyncStorage';
import {RootState} from '..';

export interface IAuthentication {
  isProcessingRequest: boolean;
  token?: string;
}

const initialState: IAuthentication = {isProcessingRequest: false};

export const authenticationSlice = createSlice({
  name: 'authentication',
  initialState,
  reducers: {
    start: state => {
      return {
        ...state,
        isProcessingRequest: true,
      };
    },
    success: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        ...action.payload,
        isProcessingRequest: false,
      };
    },
    error: (state, action: PayloadAction<any>) => {
      console.log('ERROR', action.payload, state);
      return {
        ...state,
        isProcessingRequest: false,
      };
    },
  },
});

export const authenticateUser = (userData: any) => async (dispatch: any) => {
  try {
    const authData = await authenticate(userData);
    setTokens(authData.data);
    dispatch(success({token: authData.data?.token}));
  } catch (err) {
    dispatch(error({error: err}));
  }
};

export const {start, success, error} = authenticationSlice.actions;
export const selectAuthentication = (state: RootState) => state.authentication;
export const authenticationReducer = authenticationSlice.reducer;
