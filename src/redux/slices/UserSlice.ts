import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {getUserList, getUserDetails} from '../../services/userService';
import {RootState} from '../index';
import {IUser} from '../../interfaces';

export interface IUserDetail extends IUser {
  isLoadingDetails: boolean;
}

export interface ITrack {
  [id: number]: number;
}

export interface IUsersList {
  isLoadingUsers: boolean;
  userList?: IUser[];
  userViewedList?: ITrack;
}

const userListState: IUsersList = {isLoadingUsers: false};
const userDetailState: IUserDetail = {
  isLoadingDetails: false,
  id: -1,
  email: '',
  first_name: '',
  last_name: '',
  avatar: '',
};

export const userListSlice = createSlice({
  name: 'userList',
  initialState: userListState,
  reducers: {
    listStart: state => {
      return {
        ...state,
        isLoadingUsers: true,
      };
    },
    listSuccess: (state, action: PayloadAction<any>) => {
      return {
        ...state,
        ...action.payload,
        isLoadingUsers: false,
      };
    },
    updateVisited: (state, action: PayloadAction<any>) => {
      const localViewedList = state.userViewedList;

      if (localViewedList) {
        if (action.payload?.user_id in localViewedList) {
          localViewedList[action.payload?.user_id] = 0;
        } else {
          localViewedList[action.payload?.user_id] += 1;
        }
      }

      return {
        ...state,
        userViewedList: localViewedList,
      };
    },
    listError: (state, action: PayloadAction<any>) => {
      console.error('GET_USERS_LIST_ERROR', ...action.payload);
      return {
        ...state,
        isLoadingUsers: false,
      };
    },
  },
});

export const userDetailSlice = createSlice({
  name: 'user',
  initialState: userDetailState,
  reducers: {
    start: state => {
      return {
        ...state,
        isLoadingDetails: true,
      };
    },
    success: (state, action: PayloadAction<any>) => {
      return {
        ...action.payload,
        isLoadingDetails: false,
      };
    },
    error: (state, action: PayloadAction<any>) => {
      console.error('GET_USERS_LIST_ERROR', ...action.payload);
      return {
        ...state,
        isLoadingDetails: false,
      };
    },
  },
});

export const fetchUsers = () => async (dispatch: any) => {
  dispatch(listStart());
  try {
    const userLists = await getUserList();
    dispatch(listSuccess({userList: userLists.data}));
  } catch (err) {
    dispatch(listError(err));
  }
};

export const fetchUserDetails = (user_id: number) => async (dispatch: any) => {
  dispatch(start());
  if (user_id !== -1) {
    try {
      const userDetails = await getUserDetails(user_id);
      dispatch(updateVisited({user_id: user_id}));
      dispatch(success({userDetail: userDetails.data}));
    } catch (err) {
      dispatch(error(err));
    }
  }
};

export const {listStart, listSuccess, listError, updateVisited} =
  userListSlice.actions;
export const {start, success, error} = userDetailSlice.actions;

export const selectUserLists = (state: RootState) => state.userList;
export const selectUserDetails = (state: RootState) => state.userDetails;

export const usersListReducer = userListSlice.reducer;
export const userDetailReducer = userDetailSlice.reducer;
