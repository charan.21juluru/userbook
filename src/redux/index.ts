import {configureStore, getDefaultMiddleware} from '@reduxjs/toolkit';
import {authenticationReducer} from './slices/LoginSlice';
import {userDetailReducer, usersListReducer} from './slices/UserSlice';

export const store = configureStore({
  reducer: {
    authentication: authenticationReducer,
    userList: usersListReducer,
    userDetails: userDetailReducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
