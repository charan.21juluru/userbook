import React, {useEffect, useState} from 'react';
import {useAppDispatch} from '../../hooks';
import {View, Text, StyleSheet, TextInput, Button} from 'react-native';
import {authenticateUser} from '../../redux/slices/LoginSlice';
import {isAuthenticated} from '../../services/authenticationService';

const Login = ({navigation}) => {
  const [email, setEmail] = useState<string | undefined>(() => '');
  const [password, setPassword] = useState<string | undefined>(() => '');
  const dispatch = useAppDispatch();

  useEffect(() => {
    async function checkAuth() {
      const isAuth = await isAuthenticated();
      if (isAuth) {
        navigation.replace('HomeScreen');
      }
      return isAuth;
    }

    checkAuth();
  }, [navigation]);

  const onLogin = () => {
    const user = {
      email: email,
      password: password,
    };

    dispatch(authenticateUser(user))
      .then(_ => {
        navigation.replace('HomeScreen');
      })
      .catch((error: any) => {
        console.error('LOGIN_SCREEN_ERROR', error);
        navigation.replace('LoginScreen');
      });
  };

  return (
    <View style={Styles.container}>
      <Text style={Styles.headerTitle}>Please Login to your account</Text>
      <TextInput
        style={Styles.input}
        value={email}
        onChangeText={text => setEmail(text)}
        placeholder="email"
        autoCapitalize="none"
      />
      <TextInput
        style={Styles.input}
        value={password}
        onChangeText={text => setPassword(text)}
        secureTextEntry={true}
        placeholder="password"
      />
      <Button onPress={onLogin} title="Login" />
    </View>
  );
};

export default Login;

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerTitle: {
    fontSize: 24,
  },
  input: {
    width: 360,
    fontSize: 16,
    borderWidth: 1,
    borderColor: 'gray',
    paddingVertical: 10,
    marginVertical: 10,
  },
});
