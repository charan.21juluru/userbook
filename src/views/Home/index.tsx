import React, {useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ListRenderItem,
  ActivityIndicator,
  Image,
  Pressable,
} from 'react-native';
import {useAppDispatch, useAppSelector} from '../../hooks';
import {IUser} from '../../interfaces';
import {
  fetchUserDetails,
  fetchUsers,
  IUsersList,
  selectUserLists,
} from '../../redux/slices/UserSlice';

const Home = ({navigation}) => {
  const dispatch = useAppDispatch();
  const usersList: IUsersList = useAppSelector(selectUserLists);

  useEffect(() => {
    dispatch(fetchUsers());
  }, [dispatch]);

  useEffect(() => {
    console.log(
      'USER_VISITED',
      JSON.stringify(usersList?.userViewedList, undefined, 4),
    );
  }, [usersList]);

  useEffect(() => {}, [usersList]);

  if (usersList.isLoadingUsers) {
    return <ActivityIndicator />;
  }

  const navigateToDetails = (id: number) => {
    dispatch(fetchUserDetails(id));
    navigation.navigate('DetailScreen');
  };

  const _renderItem: ListRenderItem<IUser> = ({item}) => {
    return (
      <View style={styles.container}>
        <Pressable
          style={[styles.listItemContainer]}
          onPress={() => navigateToDetails(item.id)}>
          <Image source={{uri: item.avatar}} style={styles.listItemImage} />
          <View style={styles.listItemTextContainer}>
            <Text style={styles.listItemText}>{item.email}</Text>
            <Text
              style={
                styles.listItemText
              }>{`${item.first_name} ${item.last_name}`}</Text>
          </View>
        </Pressable>
        <View style={styles.listItemSeparator} />
      </View>
    );
  };

  return <FlatList data={usersList?.userList} renderItem={_renderItem} />;
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  text: {
    fontSize: 16,
  },
  listItemContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'mediumseagreen',
  },
  listItemImage: {
    width: 100,
    height: 100,
    margin: 5,
  },
  listItemTextContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  listItemText: {
    color: 'white',
    padding: 10,
    fontSize: 16,
  },
  listItemSeparator: {
    height: 1,
    backgroundColor: 'white',
  },
});
