import React from 'react';
import {ActivityIndicator, Image, StyleSheet, Text, View} from 'react-native';
import {height, width} from '../../helpers';
import {useAppSelector} from '../../hooks';
import {selectUserDetails} from '../../redux/slices/UserSlice';

export default function Detail() {
  const userDetails: any = useAppSelector(selectUserDetails);

  if (userDetails.isLoadingDetails) {
    return <ActivityIndicator />;
  }

  return (
    !userDetails.isLoadingDetails && (
      <View style={styles.container}>
        <View style={styles.profileImageContainer}>
          <Image
            style={styles.profileImage}
            source={{uri: userDetails?.userDetail.avatar}}
          />
        </View>
        <View style={styles.profileDetailContainer}>
          <Text>{`${userDetails?.userDetail.first_name} ${userDetails?.userDetail.last_name}`}</Text>
          <Text>{userDetails?.userDetail.email}</Text>
        </View>
      </View>
    )
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
  },
  profileImageContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  profileImage: {
    height: height / 2,
    width: width / 1.1,
    backgroundColor: 'grey',
  },
  profileDetailContainer: {
    marginVertical: 16,
    flexDirection: 'column',
    alignItems: 'center',
  },
});
