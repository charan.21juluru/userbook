export interface IUser {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}
// avatar: 'https://reqres.in/img/faces/1-image.jpg';
