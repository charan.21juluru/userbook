import {Dimensions} from 'react-native';
import {getAccessToken} from '../services/asyncStorage';

export const getHeaderInfo = async () => {
  const token = await getAccessToken();
  return {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  };
};

export const width = Dimensions.get('window').width;
export const height = Dimensions.get('window').height;
